#!/bin/bash

if [ -z "$CI_COMMIT_TAG" ]; then
	exit;
fi

TAGS=()
if [[ "$CI_COMMIT_TAG" =~ ^v([[:alnum:]]+)\.([[:alnum:]]+)\.(.+)$ ]]; then
	TAGS=( "${BASH_REMATCH[1]}" "${BASH_REMATCH[1]}.${BASH_REMATCH[2]}" "${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]}" )
elif [[ "$CI_COMMIT_TAG" =~ ^v([[:alnum:]]+)\.([[:alnum:]]+)$ ]]; then
	TAGS=( "${BASH_REMATCH[1]}" "${BASH_REMATCH[1]}.${BASH_REMATCH[2]}" )
elif [[ "$CI_COMMIT_TAG" =~ ^v([[:alnum:]]+)$ ]]; then
	TAGS=( "${BASH_REMATCH[1]}" )
fi

TAG_OPTION=""
TAG_DEV_OPTION=""
for tag in "${TAGS[@]}"; do
	TAG_OPTION="$TAG_OPTION -t \"$CI_REGISTRY_IMAGE:$tag\""
	TAG_DEV_OPTION="$TAG_DEV_OPTION -t \"$CI_REGISTRY_IMAGE:$tag-dev\""
done

if [ -n "$TAG_OPTION" ]; then
	eval "docker build --pull --build-arg BUILD_ENV=production $TAG_OPTION . "
	eval "docker build --pull --build-arg BUILD_ENV=development $TAG_DEV_OPTION . "
	for tag in "${TAGS[@]}"; do
		docker push "$CI_REGISTRY_IMAGE:$tag"
		docker push "$CI_REGISTRY_IMAGE:$tag-dev"
	done
fi