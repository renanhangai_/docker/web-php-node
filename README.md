php+node container
=======================

Simple container focused on Web development. It contains:
- nginx
- php and composer
- node, npm and yarn

Basic usage
=====================

### User scripts ###

- Every file on /docker/etc/prepare.d will be executed by bash
- Every service will be started
- Every file on /docker/etc/entry.d will be executed by bash

### NGINX conf ###

- The configuration file found on $APP_ETC_DIR/httpd/default will be copied to /etc/nginx/sites-available/default
- The configuration file found on $APP_ETC_DIR/httpd/user.conf will be copied to /etc/nginx/conf.d/50-user.conf

### PHP conf ###

- The configuration file found on $APP_ETC_DIR/php/php.ini will be copied to the fpm and cli ini
- The configuration file found on $APP_ETC_DIR/php/pool.conf will be copied to .../fpm/pool.d/50-user-fpm.conf


