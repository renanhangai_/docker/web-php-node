# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.3.2...v3.0.0) (2018-12-19)


### Features

* Updated node to v10 ([dd25ea4](https://gitlab.com/renanhangai_/docker/web-php-node/commit/dd25ea4))


### BREAKING CHANGES

* Node v10 is now the base version for this img



<a name="2.3.2"></a>
## [2.3.2](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.3.1...v2.3.2) (2018-10-26)



<a name="2.3.1"></a>
## [2.3.1](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.3.0...v2.3.1) (2018-10-10)


### Bug Fixes

* Permission of the home dir ([fcf6b21](https://gitlab.com/renanhangai_/docker/web-php-node/commit/fcf6b21))



<a name="2.3.0"></a>
# [2.3.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.2.0...v2.3.0) (2018-10-07)


### Features

* Improved log from initial command ([1fb4ee3](https://gitlab.com/renanhangai_/docker/web-php-node/commit/1fb4ee3))



<a name="2.2.0"></a>
# [2.2.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.1.1...v2.2.0) (2018-10-06)


### Bug Fixes

* Fixed cleanup call for cleanup ([95c2e4a](https://gitlab.com/renanhangai_/docker/web-php-node/commit/95c2e4a))
* No more mkdir on entry.d and prepare.d ([0d13eb4](https://gitlab.com/renanhangai_/docker/web-php-node/commit/0d13eb4))


### Features

* Added template compilation for php conf ([9305f27](https://gitlab.com/renanhangai_/docker/web-php-node/commit/9305f27))
* Added template compilation for php configuration ([53b220d](https://gitlab.com/renanhangai_/docker/web-php-node/commit/53b220d))
* Automatically added link for /app/run ([95bd5bc](https://gitlab.com/renanhangai_/docker/web-php-node/commit/95bd5bc))



<a name="2.1.1"></a>
## [2.1.1](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.1.0...v2.1.1) (2018-10-03)


### Bug Fixes

* Script to clear cache was wrong ([ccec6a0](https://gitlab.com/renanhangai_/docker/web-php-node/commit/ccec6a0))



<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v2.0.0...v2.1.0) (2018-10-03)


### Features

* Added cleanup script ([ad16b31](https://gitlab.com/renanhangai_/docker/web-php-node/commit/ad16b31))



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v1.2.0...v2.0.0) (2018-09-20)


### Features

* **php:** Added configuration scripts for production and development ([d4123e5](https://gitlab.com/renanhangai_/docker/web-php-node/commit/d4123e5))
* Added template utility to copy template files using mustache bash syntax ([6f78ac8](https://gitlab.com/renanhangai_/docker/web-php-node/commit/6f78ac8))
* Default script for http is now being compiled using mo ([5165aeb](https://gitlab.com/renanhangai_/docker/web-php-node/commit/5165aeb))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v1.1.0...v1.2.0) (2018-09-18)



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/renanhangai_/docker/web-php-node/compare/v1.0.0...v1.1.0) (2018-09-16)


### Features

* Added pool configuration for php ([8ae8331](https://gitlab.com/renanhangai_/docker/web-php-node/commit/8ae8331))



<a name="1.0.0"></a>
# 1.0.0 (2018-09-15)

### Bug Fixes

* Bug when running entry scripts ([f8341d7](https://gitlab.com/renanhangai_/docker/web-php-node/commit/f8341d7))
* Command run by user was not being properly executed ([7cb1a23](https://gitlab.com/renanhangai_/docker/web-php-node/commit/7cb1a23))
* Fixed echo on sudoers file ([652e523](https://gitlab.com/renanhangai_/docker/web-php-node/commit/652e523))
* Fixed var name on dockerfile ([88d7010](https://gitlab.com/renanhangai_/docker/web-php-node/commit/88d7010))

### Features

* Added build modules and run modules for the container ([b5f67ff](https://gitlab.com/renanhangai_/docker/web-php-node/commit/b5f67ff))
* Added util wait-for-it.sh ([d1f924e](https://gitlab.com/renanhangai_/docker/web-php-node/commit/d1f924e))
