FROM ubuntu:latest

ARG BUILD_PHP_VERSION=7.2
ARG BUILD_MODULES="user node http php"
ARG BUILD_ENV="production"

ENV BUILD_PHP_VERSION $BUILD_PHP_VERSION
ENV BUILD_MODULES $BUILD_MODULES
ENV APP_DOCKER 1
ENV APP_ENV $BUILD_ENV 
ENV APP_PERMISSION_FILE "/app/local/package.json"
ENV APP_MODULES $BUILD_MODULES
ENV APP_ETC_DIR "/docker/etc/"
ENV APP_LOG_FILE "/var/log/nginx/error.log"

ADD ./scripts /docker/web-php-node/scripts
ADD ./etc /docker/web-php-node/etc
RUN ln -s /docker/web-php-node/scripts/utils /docker/utils && \
	chmod -R +x /docker/web-php-node/scripts/* && \
	/docker/web-php-node/scripts/setup && \
	/docker/web-php-node/scripts/cleanup

ENTRYPOINT [ "dumb-init", "--", "/docker/web-php-node/scripts/entry" ]
CMD []

USER me:me
WORKDIR /app/current
