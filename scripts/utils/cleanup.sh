#!/bin/bash

set +e
sudo rm -rf /var/lib/apt/lists/* || true
sudo -E yarn cache clean || true
yarn cache clean || true
sudo -E composer clearcache || true
composer clearcache || true
sudo rm -rf /tmp/* || true